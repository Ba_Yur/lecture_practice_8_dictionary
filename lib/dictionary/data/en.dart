import 'package:lecture_practice_8_dictionary/dictionary/dictionary_classes/main_page_language.dart';
import 'package:lecture_practice_8_dictionary/dictionary/models/language.dart';

const Language en = Language(
  mainPageLanguage: MainPageLanguage(
    appTitle: 'AppTitle',
  ),
);
