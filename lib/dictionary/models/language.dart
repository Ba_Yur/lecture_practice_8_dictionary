import 'package:flutter/cupertino.dart';
import 'package:lecture_practice_8_dictionary/dictionary/dictionary_classes/main_page_language.dart';

class Language {
  final MainPageLanguage mainPageLanguage;

  const Language({
    @required this.mainPageLanguage,

  });
}
