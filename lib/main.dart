import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:lecture_practice_8_dictionary/providers/language_provider.dart';
import 'package:provider/provider.dart';
import 'package:lecture_practice_8_dictionary/dictionary/data/en.dart';

import 'dictionary/dictionary_classes/main_page_language.dart';
import 'dictionary/flutter_dictionary.dart';
import 'dictionary/models/supported_locales.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<LanguageProvider>(
        create: (ctx) => LanguageProvider(),
        child: MaterialApp(
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('en', ''), // English, no country code
            const Locale('ru', ''), // Spanish, no country code
          ],
          home: MyHomePage(),
        ));
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}


class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    MainPageLanguage language =
        FlutterDictionary.instance.language?.mainPageLanguage ??
            en.mainPageLanguage;
    LanguageProvider provider = Provider.of<LanguageProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(language.appTitle),
      ),
      body: Center(
        child: DropdownButton(
          onChanged: (newValue) {
            setState(() {
              provider.setNewLane(newValue);
            });
          },
            items: SupportedLocales.instance.getSupportedLocales.map((item) {
          return DropdownMenuItem(
            child: Text(item.toString()),
            value: item,
          );
        }).toList()),
      ),
    );
  }
}
