import 'package:flutter/cupertino.dart';
import 'package:lecture_practice_8_dictionary/dictionary/flutter_dictionary.dart';

class LanguageProvider extends ChangeNotifier {
  void setNewLane(Locale locale) {
    FlutterDictionary.instance.setNewLanguage(locale.toString());
    notifyListeners();
  }
}

